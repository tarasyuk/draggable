import Muuri from  'muuri';
import select2 from  'select2';


var elem = document.querySelector('.grid');
var items = $('.js-item');
var arr = [];

// grid
$('.js-item').each(function(index, item) {
  if ($(item).hasClass('red')) {
  	arr.push(index);
  }
});
console.log(arr);
var grid = new Muuri(elem, {
  items: '.js-item',
  dragEnabled: true,
  dragStartPredicate: {
    handle: '.js-item:not(.red)'
  },
  dragSortPredicate: function(item) {
    var result = Muuri.ItemDrag.defaultSortPredicate(item);
    var index = result.index;
    return result && arr.includes(index) ? false : result;
  }
});
grid.refreshItems().layout();

// select

function formatState(state) {
  console.log(state);
  if (!state.id) {
    return state.text;
  }
  var baseUrl = 'img';
  var $state = $(
    '<span><img src="' + baseUrl + '/' + state.element.dataset.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
  );
  return $state;
};

$('.js-select').select2({
  templateSelection: formatState,
  templateResult: formatState,
  minimumResultsForSearch: -1
});
